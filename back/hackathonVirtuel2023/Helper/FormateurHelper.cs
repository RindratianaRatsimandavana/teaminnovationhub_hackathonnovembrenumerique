﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hackathonVirtuel2023.Helper
{
    public class FormateurHelper
    {
        public static formateur findFormateurParId(int idFormateur)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    var result = context.formateur.Where(x => x.id == idFormateur).FirstOrDefault();
                    return new formateur
                    {
                        id = result.id,
                        idSociete = result.idSociete,
                        mail = result.mail,
                        miniBiographie = result.miniBiographie,
                        motDePasse = result.motDePasse,
                        nom = result.nom,
                        prenom = result.prenom,
                        statut = result.statut
                    };
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static List<formateur> findFormateurParIdSociete(int idSociete)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    var result = context.formateur.Where(x => x.idSociete == idSociete).ToList();
                    List<formateur> list = new List<formateur>();
                    foreach(var item in result)
                    {
                        list.Add(new formateur
                        {
                            id = item.id,
                            idSociete = item.idSociete,
                            mail = item.mail,
                            miniBiographie = item.miniBiographie,
                            motDePasse = item.motDePasse,
                            nom = item.nom,
                            prenom = item.prenom,
                            statut = item.statut
                        });
                    }
                    return list;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}