﻿using hackathonVirtuel2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hackathonVirtuel2023.Helper
{
    public class FormationHelper
    {
        public static List<VueFormation> findAllVueFormation()
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    return context.VueFormation.ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static List<VueFormation> findVueFormationParIdCategorie(int idCategorie)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    return context.VueFormation.Where(x => x.categorie_id == idCategorie).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static apprentisEtFormation findApprentisEtFormation(int idApprentis, int idFormation)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    var result = context.apprentisEtFormation.Where(x => x.idApprentis == idApprentis && x.idFormation == idFormation).FirstOrDefault();
                    if (result != null)
                        return new apprentisEtFormation
                        {
                            id = result.id,
                            idApprentis = result.idApprentis,
                            idFormation = result.idFormation,
                            etatPaiement = result.etatPaiement
                        };
                    else
                        return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static List<VueFormation> findVueFormationParIdFormateur(int idFormateur)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    return context.VueFormation.Where(x => x.idFormateur == idFormateur).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static List<VueFormation> findVueFormationParIdSociete(int idSociete)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    return context.VueFormation.Where(x => x.idSociete == idSociete).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        // ilaina le refa eo aminfiche formations hijerena hoe inscrit ao ve sa tsia le apprentis
        public static string EtatMembreEstInscritDansFormation(int idFormation,int idUtilisateur,string email )
        {
            try
            {
                string etat = "";
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    int resulatCheck = 0;    
                    // mety misy check any  amin controller satria amin etudiant iany no mety io zavatra io
                    var retour = context.donneeCompletFormationEtApprentis
                        .Where(x => x.idApprentis == idUtilisateur && x.email == email).FirstOrDefault();
                    if (retour != null){
                        resulatCheck = 1;
                    }

                    if(resulatCheck == 1)
                    {
                        etat = "Vous êtes déjà inscrit dans cette formation";
                    }
                    return etat;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static FicheFormation findVueFormationParIdFormationPourApprentis(int idFormation,int idApprentis,string email)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    VueFormation fiche= context.VueFormation.Where(x => x.formation_id == idFormation).FirstOrDefault();
                    string etatMembre = EtatMembreEstInscritDansFormation(fiche.formation_id, idApprentis, email);
                    var ficheFormation = new FicheFormation
                    {
                        etatFormation = etatMembre,
                        VueApprentisEtFormation = fiche
                    };
                    return ficheFormation;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static VueFormation findVueFormationParIdFormation(int idFormation)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                   return context.VueFormation.Where(x => x.formation_id == idFormation).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void insertApprentisEtFormation(apprentisEtFormation apprentisEtFormation)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    context.apprentisEtFormation.Add(apprentisEtFormation);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static List<supportDeCours> findSupportDeCoursParIdFormation(int idFormation)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    return context.supportDeCours.Where(x => x.idFormation == idFormation).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        // refa makao amin mes formations
        public static List<VueApprentisEtFormation> findMesFormationsaridApprentis(int idApprentis)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    return context.VueApprentisEtFormation.Where(x => x.apprentis_id == idApprentis).ToList();
                }
            }
            catch (Exception e) { throw e; }
        }

        public static string comparaisonDateParRapportDateActuel(DateTime dateDebut, DateTime dateFin)
        {
            var retour = "";

            dateDebut = DateTimeOffset.FromUnixTimeMilliseconds(1705266000000).DateTime;
            dateFin = DateTimeOffset.FromUnixTimeMilliseconds(1705266000000).DateTime;

            // Obtenez la date actuelle
            DateTime dateActuelle = DateTime.Now;

            // Comparez la date de début avec la date actuelle
            int resultatComparaisonDateDebut = DateTime.Compare(dateDebut, dateActuelle);
            int resultatComparaisonDateFin = DateTime.Compare(dateFin, dateActuelle);

            if (resultatComparaisonDateDebut < 0)
            {
                Console.WriteLine("La date de début est antérieure à la date actuelle.");
                if(resultatComparaisonDateFin <0)
                {
                    retour = "Votre formation est déjà terminée";
                }
                else if (resultatComparaisonDateFin ==0)
                {
                    retour = "Votre formation se termine aujourd'hui";
                }
                else if (resultatComparaisonDateFin> 0)
                {
                    retour = "Votre formation est en cours";
                }

            }
            else if (resultatComparaisonDateDebut == 0)
            {
                Console.WriteLine("La date de début est la même que la date actuelle.");
                retour = "Votre formation commence aujourd'hui";
            }
            else
            {
                Console.WriteLine("La date de début est ultérieure à la date actuelle.");
                retour = "Votre formation est à venir";
            }

            return retour;
        }

        public static List<FicheMesFormations> ficheMesFormation(int idFormation)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    var result = new List<FicheMesFormations>();
                    var fiche= context.VueApprentisEtFormation.Where(x => x.formation_id == idFormation).ToList();

                    foreach(var item in fiche)
                    {
                        string resultatetat = comparaisonDateParRapportDateActuel((DateTime)item.dateDebut, (DateTime)item.dateFin);

                        FicheMesFormations ficheMesFormation = new FicheMesFormations
                        {
                            etatFormation = resultatetat,
                            VueApprentisEtFormation = item
                        };
                        result.Add(ficheMesFormation);
                    }
                    return result;
                }
            }
            catch (Exception e) { throw e; }
        }



    }
}