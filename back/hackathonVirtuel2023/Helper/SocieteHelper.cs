﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hackathonVirtuel2023.Helper
{
    public class SocieteHelper
    {
        public static societe findSocieteParId(int id)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    var result = context.societe.Where(x => x.id == id).FirstOrDefault();
                    return new societe { 
                        descriptionDetail = result.descriptionDetail,
                        id = result.id,
                        mail = result.mail,
                        nom = result.nom,
                        motDePasse = result.motDePasse,
                        statut = result.statut
                    };
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}