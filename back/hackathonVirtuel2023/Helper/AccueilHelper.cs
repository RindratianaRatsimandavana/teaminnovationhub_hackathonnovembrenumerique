﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hackathonVirtuel2023.Helper
{
    public class AccueilHelper
    {
        public static List<FAQ> findAllFaq()
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    return context.FAQ.ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}