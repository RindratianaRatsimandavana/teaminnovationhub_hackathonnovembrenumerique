﻿using hackathonVirtuel2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net.Mail;
using System.Net;

namespace hackathonVirtuel2023.Helper
{
    public class MembreHelper
    {
        public static void insertApprentis(apprentis apprentis)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    context.apprentis.Add(apprentis);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void insertSociete(societe societe)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    context.societe.Add(societe);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void insertFormateur(formateur formateur)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    context.formateur.Add(formateur);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static apprentis loginapprentis(string login, string mdp)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    var item = context.apprentis.Where(x => x.email == login && x.motDePasse == mdp).FirstOrDefault();
                    if (item != null)
                        return new apprentis
                        {
                            id = item.id,
                            nom = item.nom,
                            prenom = item.prenom,
                            adresse = item.adresse,
                            dateDeNaissance = item.dateDeNaissance,
                            email = item.email,
                            nomUtilisateur = item.nomUtilisateur,
                            numeroTelephone = item.numeroTelephone
                        };
                     return null;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static societe loginSociete(string login, string mdp)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    var item = context.societe.Where(x => x.mail == login && x.motDePasse == mdp).FirstOrDefault();
                    if (item != null)
                        return new societe
                        {
                            descriptionDetail = item.descriptionDetail,
                            id = item.id,
                            mail = item.mail,
                            nom = item.nom,
                            statut = item.statut
                        };
                    return null;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static formateur loginFormateur(string login, string mdp)
        {
            try
            {
                using (var context = new hackaton2023Entities1())
                {
                    var item = context.formateur.Where(x => x.mail == login && x.motDePasse == mdp).FirstOrDefault();
                    if (item != null)
                        return new formateur
                    {
                        id = item.id,
                        idSociete = item.idSociete,
                        mail = item.mail,
                        miniBiographie = item.miniBiographie,
                        nom = item.nom,
                        prenom = item.prenom,
                        statut = item.statut
                    };
                    return null;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static apprentis findapprentisParId(int id)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    return context.apprentis.Where(x => x.id == id).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static societe findSocieteParId(int id)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    return context.societe.Where(x => x.id == id).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static formateur findformateurParId(int id)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    return context.formateur.Where(x => x.id == id).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void updateStatutValidite(int id,int qui)
        {
            try
            {
                //String dateToday = DateTime.Now.ToString("yyyy-MM-dd");
                using (var context = new hackaton2023Entities1())
                {
                    if(qui == 1)
                    {
                        context.Database.ExecuteSqlCommand($"update societe set statut = 1 where id={id}");
                    }
                    else if (qui == 2)
                    {
                        context.Database.ExecuteSqlCommand($"update formateur set statut = 1 where id={id}");
                    }
                    //context.Database.ExecuteSqlCommand($"delete from utilisateur where id ={id}");
                   
                    //context.donneeRdvCom.SqlQuery("update rdvPris set etatRdvPris = 1 where id=" + id);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        






    }
}