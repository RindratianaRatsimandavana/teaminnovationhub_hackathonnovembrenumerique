﻿using hackathonVirtuel2023.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hackathonVirtuel2023.Controllers
{
    public class FormateurController : Controller
    {
        public JsonResult getFormateurById()
        {
            int idFormateur = int.Parse(Request["idFormateur"]);

            formateur retour = FormateurHelper.findFormateurParId(idFormateur);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getFormateurByIdSociete()
        {
            int idSociete = int.Parse(Request["idSociete"]);

            List<formateur> retour = FormateurHelper.findFormateurParIdSociete(idSociete);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }
    }
}