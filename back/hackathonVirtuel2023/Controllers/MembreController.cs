﻿using hackathonVirtuel2023.Helper;
using hackathonVirtuel2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hackathonVirtuel2023.Controllers
{

    //{
    //    "nom": "Rakoto",
    //    "prenom": "Jean",
    //    "nomUtilisateur": "rakoto.jean",
    //    "dateDeNaissance": "1990-01-15",
    //    "numeroTelephone": "0344789656",
    //    "email": "rakoto.jean@gmil.com",
    //    "adresse": "Lot 3 A Isotry"
    //}
    //https://localhost:44328/membre/inscriptionApprentis
    public class MembreController : Controller
    {
        public object MailMetier { get; private set; }

        [HttpPost]
        public ActionResult inscriptionApprentis([System.Web.Http.FromBody] apprentis apprentis)
        {
            if (apprentis != null)
            {
                string nom = apprentis.nom;
                string prenom = apprentis.prenom;
                string nomUtilisateur = apprentis.nomUtilisateur;
                //DateTime dateDeNaissance = apprentis.dateDeNaissance;
                string numeroTelephone = apprentis.numeroTelephone;
                string email = apprentis.email;
                string adresse = apprentis.adresse;

                if (string.IsNullOrWhiteSpace(nom) || string.IsNullOrWhiteSpace(prenom) ||
                    string.IsNullOrWhiteSpace(nomUtilisateur) ||
                    string.IsNullOrWhiteSpace(numeroTelephone) || string.IsNullOrWhiteSpace(adresse))
                {
                    return Json(new { Status = "Error", Message = "Un (des) champ(s) obligatoire n'est pas rempli." });
                }

                var nouveauApprenti = new apprentis
                {
                    nom = nom,
                    prenom = prenom,
                    nomUtilisateur = nomUtilisateur,
                    //dateDeNaissance = dateDeNaissance,
                    numeroTelephone = numeroTelephone,
                    email = email,
                    adresse = adresse,
                    motDePasse = apprentis.motDePasse
                };

                MembreHelper.insertApprentis(nouveauApprenti); 

                return Json(new { Status = "Success", Message = "Inscription réussie" });
            }


            return Json(new { Status = "Error", Message = "Formulaire invalide" });
        }

        //https://localhost:44328/membre/inscriptionSociete
        //{
        //    "nom": "AMAM",
        //    "descriptionDetail": "dESCRIPTION",
        //    "mail":"rindrahratsima@gmail.com" 
        //    "statut": "0"
        //}
        [HttpPost]
        public ActionResult inscriptionSociete([System.Web.Http.FromBody] societe societe)
        {
            if (societe != null)
            {
                if (string.IsNullOrWhiteSpace(societe.nom) || string.IsNullOrWhiteSpace(societe.descriptionDetail) || string.IsNullOrWhiteSpace(societe.mail))
                {
                    return Json(new { Status = "Error", Message = "Un (des) champ(s) obligatoire n'est pas rempli." });
                }

                var nouvellesociete = new societe
                {
                    nom = societe.nom,
                    descriptionDetail = societe.descriptionDetail,
                    mail = societe.mail,
                    statut = 1,
                    motDePasse = societe.motDePasse
                };

                MembreHelper.insertSociete(nouvellesociete); 

                return Json(new { Status = "Success", Message = "Inscription réussie" });
            }


            return Json(new { Status = "Error", Message = "Formulaire invalide" });
        }

        //https://localhost:44328/membre/inscriptionFormateur
        //{
        //    "nom": "Rabe",
        //    "prenom": "Manoa",
        //    "idSociete": "1",
        //    "statut": "0"
        //}
        [HttpPost]
        public ActionResult inscriptionFormateur([System.Web.Http.FromBody] formateur formateur)
        {
            if (formateur != null)
            {
                int? idSociete = formateur?.idSociete;

                if (string.IsNullOrWhiteSpace(formateur.nom) || string.IsNullOrWhiteSpace(formateur.prenom) || !idSociete.HasValue)
                {
                    return Json(new { Status = "Error", Message = "Un (des) champ(s) obligatoire n'est pas rempli." });
                }

                var nouveauFormateur = new formateur
                {
                    nom = formateur.nom,
                    prenom = formateur.prenom,
                    idSociete = (int)idSociete,
                    statut = 0,
                    motDePasse = formateur.motDePasse
                };

                MembreHelper.insertFormateur(nouveauFormateur); 

                return Json(new { Status = "Success", Message = "Inscription réussie" });
            }


            return Json(new { Status = "Error", Message = "Formulaire invalide" });
        }

        [HttpPost]
        public ActionResult loginFormateur([System.Web.Http.FromBody] LoginForm LoginForm)
        {
            if (LoginForm != null)
            {
                if (string.IsNullOrWhiteSpace(LoginForm.login) || string.IsNullOrWhiteSpace(LoginForm.motDePasse))
                {
                    return Json(new { Status = "Error", Message = "Un (des) champ(s) obligatoire n'est pas rempli." });
                }

                
                formateur retour = MembreHelper.loginFormateur(LoginForm.login, LoginForm.motDePasse);

                if (retour != null)
                {
                    
                    return Json(new { Status = "Success", Message = "Connexion réussie", data = retour });
                }
                else
                {
                    return Json(new { Status = "Error", Message = "Identifiants incorrects" });
                }
            }

            return Json(new { Status = "Error", Message = "Formulaire invalide" });
        }

        public ActionResult loginSociete([System.Web.Http.FromBody] LoginForm LoginForm)
        {
            if (LoginForm != null)
            {
                if (string.IsNullOrWhiteSpace(LoginForm.login) || string.IsNullOrWhiteSpace(LoginForm.motDePasse))
                {
                    return Json(new { Status = "Error", Message = "Un (des) champ(s) obligatoire n'est pas rempli." });
                }

                
                societe retour = MembreHelper.loginSociete(LoginForm.login, LoginForm.motDePasse);

                if (retour != null)
                {
                
                    return Json(new { Status = "Success", Message = "Connexion réussie", data = retour });
                }
                else
                {
                    return Json(new { Status = "Error", Message = "Identifiants incorrects" });
                }
            }

            return Json(new { Status = "Error", Message = "Formulaire invalide" });
        }

        public ActionResult loginApprenti([System.Web.Http.FromBody] LoginForm LoginForm)
        {
            if (LoginForm != null)
            {
                if (string.IsNullOrWhiteSpace(LoginForm.login) || string.IsNullOrWhiteSpace(LoginForm.motDePasse))
                {
                    return Json(new { Status = "Error", Message = "Un (des) champ(s) obligatoire n'est pas rempli." });
                }

               
                apprentis retour = MembreHelper.loginapprentis(LoginForm.login, LoginForm.motDePasse);

                if (retour != null)
                {
                   
                    return Json(new { Status = "Success", Message = "Connexion réussie", data = retour });
                }
                else
                {
                    return Json(new { Status = "Error", Message = "Identifiants incorrects" });
                }
            }

            return Json(new { Status = "Error", Message = "Formulaire invalide" });
        }



        public ActionResult validerFormateur()
        {
            int idAmodifier = int.Parse(Request["idAmodifier"]);
            
            //societe societe = MembreHelper.findSocieteParId(idAmodifier);
           
            MembreHelper.updateStatutValidite(idAmodifier, 2);
            return Json(new { Status = "Success", Message = "Validation réussie" });
        }
    }
}

       
       

   

        

