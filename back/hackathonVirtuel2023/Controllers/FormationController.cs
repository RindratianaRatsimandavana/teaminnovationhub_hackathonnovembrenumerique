﻿using hackathonVirtuel2023.Helper;
using hackathonVirtuel2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hackathonVirtuel2023.Controllers
{
    public class FormationController : Controller
    {
        public JsonResult getAllFormation() 
        {
            List<VueFormation> retour = FormationHelper.findAllVueFormation();
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult insertApprentisEtFormation([System.Web.Http.FromBody] apprentisEtFormation apprentiFormation)
        {
            apprentiFormation.etatPaiement = 1;
            FormationHelper.insertApprentisEtFormation(apprentiFormation);
            return Json(new { Status = "Success", Message = "Inscription réussie" });
        }

        public JsonResult getApprentisEtFormation()
        {
            int idApprentis = int.Parse(Request["idApprentis"]);
            int idFormation = int.Parse(Request["idFormation"]);

            apprentisEtFormation retour = FormationHelper.findApprentisEtFormation(idApprentis, idFormation);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getVueFormationByIdCategorie()
        {
            int idCategorie = int.Parse(Request["idCategorie"]);

            List<VueFormation> retour= FormationHelper.findVueFormationParIdCategorie(idCategorie);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getVueFormationByIdFormateur()
        {
            int idFormateur = int.Parse(Request["idFormateur"]);

            List<VueFormation> retour = FormationHelper.findVueFormationParIdFormateur(idFormateur);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getVueFormationByIdSociete()
        {
            int idSociete = int.Parse(Request["idSociete"]);

            List<VueFormation> retour = FormationHelper.findVueFormationParIdCategorie(idSociete);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getVueFormationById()
        {
            int idFormation = int.Parse(Request["idFormation"]);
            string email = Request["email"];
            int idApprentis = int.Parse(Request["idApprentis"]);
            if((email != null))
            {
                FicheFormation retour1 = FormationHelper.findVueFormationParIdFormationPourApprentis(idFormation,idApprentis,email);
                return Json(retour1, JsonRequestBehavior.AllowGet);
            }
            VueFormation retour = FormationHelper.findVueFormationParIdFormation(idFormation);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSupportDeCoursByIdFormation()
        {
            int idFormation = int.Parse(Request["idFormation"]);

            List<supportDeCours> retour = FormationHelper.findSupportDeCoursParIdFormation(idFormation);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMesFormationsParIdApprenti()
        {
            int idApprentis = int.Parse(Request["idApprentis"]);

            var retour = FormationHelper.ficheMesFormation(idApprentis);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }
    }
}