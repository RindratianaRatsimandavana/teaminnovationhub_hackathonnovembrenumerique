﻿using hackathonVirtuel2023.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hackathonVirtuel2023.Controllers
{
    public class SocieteController : Controller
    {
        public object SocietHelper { get; private set; }

        public JsonResult getSocieteById()
        {
            int idSociete = int.Parse(Request["idSociete"]);

            societe retour = SocieteHelper.findSocieteParId(idSociete);
            return Json(retour, JsonRequestBehavior.AllowGet);
        }
    }
}