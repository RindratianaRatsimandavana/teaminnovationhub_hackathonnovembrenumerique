﻿using hackathonVirtuel2023.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hackathonVirtuel2023.Controllers
{
    public class AccueilController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult getFAQ()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult getAllFAQ() 
        {
            List<FAQ> retour = AccueilHelper.findAllFaq();
            return Json(retour, JsonRequestBehavior.AllowGet);
        }




    

    }
}