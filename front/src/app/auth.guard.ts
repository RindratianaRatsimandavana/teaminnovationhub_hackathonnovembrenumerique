import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthService } from "./services/auth.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  USER_KEY = "auth-user";

  canActivate(): boolean {
    // if (this.authService.isLoggedIn) {
    //   return true;
    // }
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return true;
    }
    // this.router.navigate(["connexion"]);
    this.router.navigate(["accueil"]);
    return false;
  }
}
