import { Component } from "@angular/core";

@Component({
  selector: "app-page-not-found",
  template: `
<div class="">
      <h2>SORRY, 404 PAGE NOT FOUND😕😕😕</h2>
</div>
  `,
  styleUrls: ["page-not-found.component.css"],
})
export class PageNotFoundComponent {}
