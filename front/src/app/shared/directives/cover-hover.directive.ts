import { Directive, ElementRef, HostListener } from "@angular/core";

@Directive({
  standalone: true,
  selector: "[flixCoverHover]"
})
export class CoverHoverDirective {
  /* cursor */
  private defaultCursor: string = "pointer";
  /* Scale */
  private initialTransform: number = 1;
  private defaultTransform: number = 1.02;
  /* Transition */
  private defaultSelect: string = "all";
  private defaultTime: number = 0.325;
  private defaultTransition: string = "ease-in-out";

  constructor(private el: ElementRef) {
    this.setCursor(this.defaultCursor);
    this.setTransform(this.initialTransform);
    this.setTransition(
      this.defaultSelect,
      this.defaultTime,
      this.defaultTransition
    );
  }

  @HostListener("mouseenter")
  onMouseEnter() {
    this.setCursor(this.defaultCursor);
    this.setTransform(this.defaultTransform);
    this.setTransition(
      this.defaultSelect,
      this.defaultTime,
      this.defaultTransition
    );
    this.setZIndex(1);
  }

  @HostListener("mouseleave")
  onMouseLeave() {
    this.setTransform(this.initialTransform);
    this.setZIndex(0);
  }

  private setCursor(cursor: string) {
    this.el.nativeElement.style.cursor = `${cursor}`;
  }

  private setTransform(transform: number) {
    this.el.nativeElement.style.transform = `scale(${transform})`;
  }

  private setTransition(select: string, time: number, transition: string) {
    this.el.nativeElement.style.transition = `${select} ${time}s ${transition}`;
  }

  private setZIndex(zIndex: number) {
    this.el.nativeElement.style.zIndex = zIndex.toString();
  }
}
