import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "./material.module";
import { ShortenPipe } from "./pipes/shorten.pipe";
import { BarComponent } from "./chart/bar/bar.component";
import { DonutComponent } from "./chart/donut/donut.component";
import { LineComponent } from "./chart/line/line.component";
import { PieComponent } from "./chart/pie/pie.component";

@NgModule({
  declarations: [
    ShortenPipe,
    BarComponent,
    DonutComponent,
    LineComponent,
    PieComponent
  ],
  imports: [CommonModule],
  exports: [
    MaterialModule,
    ShortenPipe,
    BarComponent,
    DonutComponent,
    LineComponent,
    PieComponent
  ]
})
export class SharedModule {}
