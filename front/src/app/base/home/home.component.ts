import { Component, OnInit } from "@angular/core";
import { NgbCarouselConfig } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { FAQsService } from "src/app/services/faqs.service";
import { ScrollService } from "src/app/services/scroll.service";
import * as AOS from "aos";
import { ScrollTopService } from "src/app/services/scrollTop.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit {
  isMenuCollapsed: boolean = true;
  data: any;
  showNavigationArrows = false;
  showNavigationIndicators = false;
  userData: any;
  USER_KEY = "auth-user";

  images = [
    {
      link: "../../../assets/img/carousel/1.jpg",
      title: "Partenariat Stratégique",
      content:
        "Rejoignez notre réseau en tant que partenaire stratégique pour former et développer une équipe de collaborateurs fiables et compétents. En travaillant ensemble, nous visons à établir des relations professionnelles solides, à partager les meilleures pratiques du secteur et à créer un environnement où l'innovation et la croissance sont encouragées.",
    },
    {
      link: "../../../assets/img/carousel/2.jpg",
      title: "Projet d'Excellence Professionnelle",
      content:
        "Explorez des projets d'excellence professionnelle et de croissance de carrière en intégrant notre plateforme. Nous vous offrons des opportunités stimulantes dans divers secteurs, ainsi qu'un accompagnement personnalisé pour maximiser votre potentiel professionnel. Joignez-vous à nous pour sculpter un avenir professionnel exceptionnel.",
    },
    {
      link: "../../../assets/img/carousel/3.jpg",
      title: "Communauté de Savoir et de Partage",
      content:
        "Que vous soyez un expert chevronné ou un novice curieux, notre plateforme offre une communauté dynamique centrée sur le savoir et le partage. Profitez d'espaces dédiés à la discussion, de sessions de formation innovantes, et contribuez à façonner une communauté où la curiosité intellectuelle est célébrée. Rejoignez-nous pour faire partie d'une communauté tissée par la passion du savoir.",
    },
  ];

  constructor(
    private faqService: FAQsService,
    private config: NgbCarouselConfig,
    private authService: AuthService,
    private scrollService: ScrollService,
    private scrollTop: ScrollTopService,
    private router: Router
  ) {
    // customize default values of carousels used by this component tree
    this.config.showNavigationArrows = true;
    this.config.showNavigationIndicators = true;
    this.setUserData();
  }

  ngOnInit() {
    this.scrollTop.onTop();
    AOS.init();
    this.getFaq();
  }

  goToLogIn(type: string) {
    this.router.navigate([`connexion/${type}`]);
  }

  goToLogout() {
    this.authService.logout();
    this.router.navigate(["accueil"]);
  }

  goToInscriptionSociete() {
    this.router.navigate(["inscription-societe"]);
  }

  goToInscriptionApprentis() {
    this.router.navigate(["inscription-apprentis"]);
  }

  setUserData() {
    this.userData = this.authService.getUser();
  }

  isLoggedIn() {
    this.setUserData();
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return true;
    }
    return false;
  }

  getFaq() {
    this.faqService.getAllFAQ().subscribe((response) => {
      this.data = response;
    });
  }

  scrollToSection(sectionId: string): void {
    this.scrollService.scrollToSection(sectionId);
  }
}
