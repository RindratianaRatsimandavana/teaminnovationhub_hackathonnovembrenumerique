import { animate, style, transition, trigger } from "@angular/animations";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { ScrollService } from "src/app/services/scroll.service";

@Component({
  selector: "app-header",
  templateUrl: "header.component.html",
  styleUrls: ["header.component.css"],
  animations: [
    trigger("scrollAnimation", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateY(-50px)" }),
        animate("500ms", style({ opacity: 1, transform: "translateY(0)" })),
      ]),
      transition(":leave", [
        style({ opacity: 1, transform: "translateY(0)" }),
        animate("500ms", style({ opacity: 0, transform: "translateY(-50px)" })),
      ]),
    ]),
  ],
})
export class HeaderComponent implements OnInit {
  isMenuCollapsed: boolean = true;
  userData: any;
  typeUserData: any;
  USER_KEY = "auth-user";
  cmt: any;
  TYPE_USER_KEY = "type-user";

  constructor(
    private router: Router,
    private authService: AuthService,
    private scrollService: ScrollService
  ) {
    this.setUserData();
  }
  ngOnInit(): void {}

  // goToLogIn() {
  //   this.router.navigate(["connexion"]);
  // }

  goToLogout() {
    this.authService.logout();
    this.router.navigate([`connexion/${this.typeUserData}`]);
  }

  goToSignIn() {
    this.router.navigate(["s-inscrire"]);
  }

  goToDashboard() {
    this.router.navigate(["tableau-de-bord"]);
  }

  setUserData() {
    this.userData = this.authService.getUser();
    this.typeUserData = this.authService.getTypeUser();
  }

  isLoggedIn() {
    this.setUserData();
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return true;
    }
    return false;
    // return this.authService.isLoggedIn();
  }

  scrollToSection(sectionId: string): void {
    this.scrollService.scrollToSection(sectionId);
  }
}
