import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ScrollTopService } from "src/app/services/scrollTop.service";
import { ToastService } from "src/app/services/toast.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-inscription-societe",
  templateUrl: "./inscription-societe.component.html",
  styleUrls: ["./inscription-societe.component.css"],
})
export class InscriptionSocieteComponent implements OnInit {
  nom: string;
  descriptionDetail: string;
  mail: string;
  motDePasse: string;
  motDePasse1: string;

  constructor(
    private scrollTop: ScrollTopService,
    private userService: UserService,
    private router: Router,
    private toastService: ToastService
  ) {}
  ngOnInit(): void {
    this.scrollTop.onTop();
  }

  inscription() {
    this.userService
      .inscriptionSociete(
        this.nom,
        this.descriptionDetail,
        this.mail,
        this.motDePasse
      )
      .subscribe((data: any) => {
        console.log(data);
        this.toastService.show(
          "Votre inscription a été effectué avec success",
          {
            classname: "bg-success text-light",
            delay: 2000,
          }
        );
        this.router.navigate(["connexion/societe"]);
      });
  }
}
