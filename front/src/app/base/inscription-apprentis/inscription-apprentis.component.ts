import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ScrollTopService } from "src/app/services/scrollTop.service";
import { ToastService } from "src/app/services/toast.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-inscription-apprentis",
  templateUrl: "./inscription-apprentis.component.html",
  styleUrls: ["./inscription-apprentis.component.css"],
})
export class InscriptionApprentisComponent implements OnInit {
  nom: string;
  prenom: string;
  nomUtilisateur: string;
  dateDeNaissance: string;
  numeroTelephone: string;
  email: string;
  adresse: string;
  motDePasse: string;
  motDePasse1: string;

  constructor(
    private scrollTop: ScrollTopService,
    private userService: UserService,
    private router: Router,
    private toastService: ToastService
  ) {}
  ngOnInit(): void {
    this.scrollTop.onTop();
  }

  inscription() {
    this.userService
      .inscriptionApprentis(
        this.nom,
        this.prenom,
        this.nomUtilisateur,
        this.dateDeNaissance,
        this.numeroTelephone,
        this.email,
        this.adresse,
        this.motDePasse
      )
      .subscribe((data: any) => {
        console.log(data);
        this.toastService.show(
          "Votre inscription a été effectué avec success",
          {
            classname: "bg-success text-light",
            delay: 2000,
          }
        );
        this.router.navigate(["connexion/apprentis"]);
      });
  }
}
