import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LogInComponent } from "./log-in/log-in.component";
import { InscriptionSocieteComponent } from "./inscription-societe/inscription-societe.component";
import { FormsModule } from "@angular/forms";
import {
  NgbAccordionModule,
  NgbCollapseModule,
  NgbDropdownModule,
  NgbCarouselModule,
  NgbCarouselConfig,
} from "@ng-bootstrap/ng-bootstrap";
import { FAQsService } from "../services/faqs.service";
import { FooterComponent } from "./footer/footer.component";
import { InscriptionApprentisComponent } from "./inscription-apprentis/inscription-apprentis.component";

const baseRoutes: Routes = [
  { path: "inscription-societe", component: InscriptionSocieteComponent },
  { path: "inscription-apprentis", component: InscriptionApprentisComponent },
  { path: "connexion/:type", component: LogInComponent },
  { path: "accueil", component: HomeComponent },
];

@NgModule({
  declarations: [
    LogInComponent,
    InscriptionSocieteComponent,
    HomeComponent,
    InscriptionApprentisComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbDropdownModule,
    NgbAccordionModule,
    NgbCollapseModule,
    NgbCarouselModule,
    RouterModule.forChild(baseRoutes),
  ],
  providers: [FAQsService, NgbCarouselConfig],
})
export class BaseModule {}
