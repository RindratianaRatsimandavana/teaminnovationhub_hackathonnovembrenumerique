import { OnInit } from "@angular/core";
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { ScrollTopService } from "src/app/services/scrollTop.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-log-in",
  templateUrl: "./log-in.component.html",
  styleUrls: ["./log-in.component.css"],
})
export class LogInComponent implements OnInit {
  message: string =
    "Entrer votre identifiant et mot de passe. Ne vous inquetiez pas je ne vois rien🫣";
  username: string;
  password: string;
  mdpLostMessage: string;
  type = this.route.snapshot.params["type"];
  API_URL = "https://localhost:44329/";
  USER_KEY = "auth-user";
  TYPE_USER_KEY = "type-user";

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private scrollTop: ScrollTopService

  ) {}
  ngOnInit(): void {
    this.scrollTop.onTop();
  }

  setMessage() {
    if (this.authService.isLoggedIn) {
      this.message = "Vous êtes connecté";
    } else {
      this.message = "Identifiant ou mot de passe incorrect 🥺";
    }
  }

  logIn() {
    this.message = "Tentative de connexion en cours...";

    // const isLoggedIn = this.authService.login(
    //   this.username,
    //   this.password,
    //   this.type
    // );
    // console.log("isLoggedIn:" + isLoggedIn);
    // if (isLoggedIn) {
    //   this.router.navigate(["formation/list-formation"]);
    // } else {
    //   this.password = "";
    //   this.router.navigate([`connexion/${this.type}`]);
    // }

    // this.authService
    //   .login(this.username, this.password)
    //   .subscribe((isLoggedIn: boolean) => {
    //     this.setMessage();
    //     if (isLoggedIn) {
    //       this.router.navigate(["formation/list-formation"]);
    //     } else {
    //       this.password = "";
    //       this.router.navigate(["connexion"]);
    //     }
    //   });

    this.userService
      .login(this.username, this.password, this.type)
      .subscribe((data: any) => {
        this.setMessage();
        let userConnecte = data.data as any[];
        if (userConnecte !== null && userConnecte !== undefined) {
          window.sessionStorage.removeItem(this.USER_KEY);
          window.sessionStorage.setItem(
            this.USER_KEY,
            JSON.stringify(userConnecte)
          );
          window.sessionStorage.setItem(this.TYPE_USER_KEY, this.type);
          this.router.navigate(["formation/list-formation"]);
        } else {
          this.password = "";
          this.router.navigate(["accueil"]);
        }
      });
  }

  logOut() {
    this.authService.logout();
    this.message = "Vous êtes déconnecté";
  }

  mdpLost() {
    if (this.mdpLostMessage) {
      this.mdpLostMessage = "";
    } else {
      this.mdpLostMessage =
        "Prener un peu l'air, et j'espère bien que vous allez vous en souvenir";
    }
  }
}
