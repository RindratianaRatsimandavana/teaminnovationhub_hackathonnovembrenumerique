import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbNavChangeEvent } from "@ng-bootstrap/ng-bootstrap";
import * as AOS from "aos";

@Component({
  selector: "app-quiz-question",
  templateUrl: "./quiz-question.component.html",
  styleUrls: ["quiz-question.component.css"]
})
export class QuizQuestionComponent implements OnInit {
  active = 1;
  answers: any = {}; // Stocke les réponses aux questions

  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {
    AOS.init();
  }

  onNavChange(event: any) {
    // Vous pouvez ajouter une logique supplémentaire ici si nécessaire
  }

  submitQuiz() {
    // Vous pouvez implémenter la logique de soumission ici
    console.log(this.answers);
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }
}
