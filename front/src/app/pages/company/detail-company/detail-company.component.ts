import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { SocieteService } from "src/app/services/societe.service";

@Component({
  selector: "app-detail-company",
  templateUrl: "./detail-company.component.html",
  styleUrls: ["./detail-company.component.css"],
})
export class DetailCompanyComponent {
  idSociete = this.route.snapshot.params["id"];
  userData: any;
  dataSociete: any;

  constructor(
    private route: ActivatedRoute,
    private societeService: SocieteService,
    private authService: AuthService
  ) {
    this.setUserData();
    this.getSocieteDetails();
  }

  ngOnInit(): void {}

  setUserData() {
    this.userData = this.authService.getUser();
  }

  getSocieteDetails() {
    this.societeService.getSocieteById(this.idSociete).subscribe((response) => {
      this.dataSociete = response;
    });
  }
}
