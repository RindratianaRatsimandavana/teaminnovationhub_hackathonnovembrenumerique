import { Component, OnInit } from "@angular/core";

import * as AOS from "aos";

@Component({
  selector: "app-societe",
  templateUrl: "./societe-dashboard.component.html",
  styleUrls: ["societe-dashboard.component.css"]
})
export class SocieteDashboardComponent implements OnInit {
  ngOnInit(): void {
    AOS.init();
  }
}
