import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthService } from "src/app/services/auth.service";
import { FormateurService } from "src/app/services/formateur.service";
import { FormationService } from "src/app/services/formation.service";
import * as AOS from "aos";
import { ScrollTopService } from "src/app/services/scrollTop.service";

@Component({
  selector: "app-detail-formateur",
  templateUrl: "./detail-formateur.component.html",
  styleUrls: ["./detail-formateur.component.css"]
})
export class DetailFormateurComponent {
  idFormateur = this.route.snapshot.params["id"];
  userData: any;
  dataFormateur: any;

  constructor(
    private route: ActivatedRoute,
    private formateurService: FormateurService,
    private scrollTop: ScrollTopService,
    private authService: AuthService,
  ) {
    this.setUserData();
    this.getFormateurDetails();
  }

  ngOnInit(): void {
    this.scrollTop.onTop();
    AOS.init();
  }

  setUserData() {
    this.userData = this.authService.getUser();
  }

  getFormateurDetails() {
    this.formateurService
      .getFormateurById(this.idFormateur)
      .subscribe(response => {
        this.dataFormateur = response;
      });
  }
}
