import { ScrollTopService } from "./../../../services/scrollTop.service";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthService } from "src/app/services/auth.service";
import { FormationService } from "src/app/services/formation.service";
import { ToastService } from "src/app/services/toast.service";
import * as AOS from "aos";

@Component({
  selector: "app-detail-formation",
  templateUrl: "./detail-formation.component.html",
  styleUrls: ["./detail-formation.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class DetailFormationComponent implements OnInit {
  idFormation = this.route.snapshot.params["id"];
  userData: any;
  typeUserData: any;
  dataFormation: any;
  apprentisEtFormation: any;
  USER_KEY = "auth-user";

  apprentis = [
    { img: "assets/avatar/1.jpg",nom: "RAKOTOVAO" },
    { img: "assets/avatar/2.jpg", nom: "RATONGASOA" },
    { img: "assets/avatar/3.jpg", nom: "ANDRIANJAFY" },
    { img: "assets/avatar/4.jpg", nom: "NOMENJANAHARY" }
  ];

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private formationService: FormationService,
    private authService: AuthService,
    private toastService: ToastService,
    private scrollTop: ScrollTopService
  ) {
    this.setUserData();
    this.getFormationDetails();
    this.getApprentisEtFormation();
  }
  ngOnInit(): void {
    this.scrollTop.onTop();
    AOS.init();
  }

  setUserData() {
    this.userData = this.authService.getUser();
    this.typeUserData = this.authService.getTypeUser();
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  goToFormateur(idFormateur: string) {
    this.router.navigate([`formateur/${idFormateur}`]);
  }

  getFormationDetails() {
    this.formationService
      .getVueFormationByIdFormationAndIdApprenti(
        this.idFormation,
        this.userData.id
      )
      .subscribe(response => {
        this.dataFormation = response;
      });
  }

  getApprentisEtFormation() {
    this.formationService
      .getApprentisEtFormation(this.userData.id, this.idFormation)
      .subscribe(response => {
        this.apprentisEtFormation = response;
        console.log(response);
      });
  }

  insertApprentisEtFormation(idFormation: number) {
    this.formationService
      .insertApprentisEtFormation(this.userData.id, idFormation)
      .subscribe((data: any) => {
        this.getApprentisEtFormation();
        this.toastService.show("Votre Paiement a été effectué avec success", {
          classname: "bg-success text-light",
          delay: 2000
        });
      });
  }

  isLoggedIn() {
    this.setUserData();
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return true;
    }
    return false;
    // return this.authService.isLoggedIn();
  }
}
