import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { CategoryService } from "src/app/services/category.service";
import { FormationService } from "src/app/services/formation.service";
import * as AOS from "aos";
import * as moment from "moment";

@Component({
  selector: "app-list-formation",
  templateUrl: "./list-formation.component.html",
  styleUrls: ["./list-formation.component.css"],
})
export class ListFormationComponent implements OnInit {
  dataFormation: any;
  dataApprentiFormation: any;
  dataCategorie: any;
  idCategorie: number;
  userData: any;
  typeUserData: any;
  USER_KEY = "auth-user";

  constructor(
    private router: Router,
    private formationService: FormationService,
    private categoryService: CategoryService,
    private authService: AuthService
  ) {
    this.setUserData();
  }
  ngOnInit() {
    // this.dataFormation = this.formationService.jsonData;
    AOS.init();
    this.dataCategorie = this.categoryService.jsonData;
    this.getFormations();
  }

  isLoggedIn() {
    this.setUserData();
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return true;
    }
    return false;
    // return this.authService.isLoggedIn();
  }

  setUserData() {
    this.userData = this.authService.getUser();
    this.typeUserData = this.authService.getTypeUser();
  }

  getFormations() {
    switch (this.typeUserData) {
      case "apprentis":
        this.formationService.getAllFormation().subscribe((response) => {
          this.dataFormation = response;
        });
        this.formationService
          .getMesFormationsParIdApprenti(this.userData.id)
          .subscribe((response) => {
            this.dataApprentiFormation = response;
          });
        break;
      case "societe":
        this.formationService
          .getAllFormationParSociete(this.userData.id)
          .subscribe((response) => {
            this.dataFormation = response;
          });
        break;
      case "formateur":
        this.formationService
          .getAllFormationParSociete(this.userData.id)
          .subscribe((response) => {
            this.dataFormation = response;
          });
        break;
    }
  }

  refreshFormations(idCategorie: number) {
    if (idCategorie === 0) this.getFormations();
    else
      this.formationService
        .getVueFormationByIdCategorie(idCategorie)
        .subscribe((response) => {
          this.dataFormation = response;
          this.idCategorie = idCategorie;
        });
  }

  gotToDetailFormation(idFormation: string) {
    this.router.navigate([`formation/${idFormation}`]);
  }

  goToCompanyProfile(idCompany: string) {
    this.router.navigate([`societe/${idCompany}`]);
  }

  formatDate(dateValue: number) {
    return moment(dateValue).format("DD-MM-YYYY");
  }
}
