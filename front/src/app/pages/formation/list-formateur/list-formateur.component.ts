import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { CategoryService } from "src/app/services/category.service";
import { FormateurService } from "src/app/services/formateur.service";

@Component({
  selector: "app-list-formateur",
  templateUrl: "./list-formateur.component.html",
  styleUrls: ["./list-formateur.component.css"],
})
export class ListFormateurComponent implements OnInit {
  dataFormateur: any;
  userData: any;
  typeUserData: any;
  idSociete = this.route.snapshot.params["idSociete"];
  USER_KEY = "auth-user";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formateurService: FormateurService,
    private authService: AuthService
  ) {
    this.setUserData();
    this.getFormateurs();
  }
  ngOnInit(): void {}

  isLoggedIn() {
    this.setUserData();
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return true;
    }
    return false;
    // return this.authService.isLoggedIn();
  }

  setUserData() {
    this.userData = this.authService.getUser();
    this.typeUserData = this.authService.getTypeUser();
  }

  getFormateurs() {
    this.formateurService
      .getFormateurByIdSociete(this.idSociete)
      .subscribe((response) => {
        this.dataFormateur = response;
        console.log(this.dataFormateur);
      });
  }

  goToFormateur(idFormateur: string) {
    this.router.navigate([`formateur/${idFormateur}`]);
  }

  validerFormateur(idFormateur: number) {
    this.formateurService
      .validerFormateur(idFormateur)
      .subscribe((response) => {
        // this.getFormateurs();
        this.router.navigate([`formation/list-formateur/${this.userData.id}`]);
      });
  }
}
