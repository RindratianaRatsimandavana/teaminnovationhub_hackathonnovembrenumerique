import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { ListFormationComponent } from "./formation/list-formation/list-formation.component";
import { AuthGuard } from "../auth.guard";
import { DetailFormationComponent } from "./formation/detail-formation/detail-formation.component";
import { FormationService } from "../services/formation.service";
import { SharedModule } from "../shared/shared.module";
import { CategoryService } from "../services/category.service";
import { DetailCompanyComponent } from "./company/detail-company/detail-company.component";
import { BlogComponent } from "./blog/blog/blog.component";
import { BlogDetailComponent } from "./blog/blog-detail/blog-detail.component";
import { QuizQuestionComponent } from "./quiz/quiz-question/quiz-question.component";
import { FormsModule } from "@angular/forms";
import { DetailFormateurComponent } from "./formation/detail-formateur/detail-formateur.component";
import {
  NgbAccordionModule,
  NgbNavModule,
  NgbPaginationModule,
  NgbToastModule,
  NgbTooltipModule
} from "@ng-bootstrap/ng-bootstrap";
import { ToastService } from "../services/toast.service";
import { ToastsContainer } from "./formation/detail-formation/toasts-container.component";
import { CoverHoverDirective } from "../shared/directives/cover-hover.directive";
import { ListFormateurComponent } from "./formation/list-formateur/list-formateur.component";
import { SocieteDashboardComponent } from "./dashboard/societe/societe-dashboard.component";

const baseRoutes: Routes = [
  {
    path: "tableau-de-bord",
    component: SocieteDashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "formation/list-formation",
    component: ListFormationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "formation/list-formateur/:idSociete",
    component: ListFormateurComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "blog",
    component: BlogComponent
  },
  {
    path: "quiz",
    component: QuizQuestionComponent
  },
  {
    path: "blog/:id",
    component: BlogDetailComponent
  },
  {
    path: "formation/:id",
    component: DetailFormationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "societe/:id",
    component: DetailCompanyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "formateur/:id",
    component: DetailFormateurComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [
    ListFormationComponent,
    ListFormateurComponent,
    DetailFormationComponent,
    DetailCompanyComponent,
    BlogDetailComponent,
    BlogComponent,
    QuizQuestionComponent,
    DetailFormateurComponent,
    ToastsContainer,
    SocieteDashboardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    CoverHoverDirective,
    NgbTooltipModule,
    NgbToastModule,
    NgbAccordionModule,
    NgbPaginationModule,
    NgbNavModule,
    RouterModule.forChild(baseRoutes)
  ],
  providers: [FormationService, CategoryService, ToastService],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PageModule {}
