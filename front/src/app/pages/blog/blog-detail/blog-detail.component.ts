import { Component, OnInit } from "@angular/core";
import * as AOS from "aos";

@Component({
  selector: "app-blog-detail",
  templateUrl: "./blog-detail.component.html",
  styleUrls: ["./blog-detail.component.css"]
})
export class BlogDetailComponent implements OnInit {
  art = {
    id: 0,
    titre: "L'importance de l'éducation continue dans le monde professionnel",
    auteur: "John Doe",
    date: "2023-11-10",
    contenu:
      "L'éducation ne devrait pas s'arrêter une fois que vous avez obtenu votre diplôme. Dans le monde professionnel d'aujourd'hui, l'apprentissage continu est essentiel pour rester compétent et relevant. Cet article explore l'importance de l'éducation continue et comment elle peut influencer votre carrière.L'éducation ne devrait pas s'arrêter une fois que vous avez obtenu votre diplôme. Dans le monde professionnel d'aujourd'hui, l'apprentissage continu est essentiel pour rester compétent et relevant. Cet article explore l'importance de l'éducation continue et comment elle peut influencer votre carrière. L'éducation ne devrait pas s'arrêter une fois que vous avez obtenu votre diplôme. Dans le monde professionnel d'aujourd'hui, l'apprentissage continu est essentiel pour rester compétent et relevant. Cet article explore l'importance de l'éducation continue et comment elle peut influencer votre carrière.L'éducation ne devrait pas s'arrêter une fois que vous avez obtenu votre diplôme. Dans le monde professionnel d'aujourd'hui, l'apprentissage continu est essentiel pour rester compétent et relevant. Cet article explore l'importance de l'éducation continue et comment elle peut influencer votre carrière.",
    tags: [
      "éducation continue",
      "formation professionnelle",
      "développement personnel"
    ]
  };
  articles = [
    {
      id: 1,
      titre:
        "Les avantages des formations en ligne pour les professionnels occupés",
      auteur: "Jane Smith",
      date: "2023-11-12",
      contenu:
        "La vie professionnelle peut être trépidante, mais cela ne devrait pas vous empêcher de poursuivre votre développement. Les formations en ligne offrent une solution flexible pour les professionnels occupés. Découvrez les avantages de ces cours virtuels et comment ils peuvent améliorer vos compétences.\n\n...",
      tags: [
        "formations en ligne",
        "flexibilité",
        "développement professionnel"
      ]
    },
    {
      id: 2,
      titre: "Construire une carrière réussie grâce à la formation continue",
      auteur: "Alice Dupont",
      date: "2023-11-15",
      contenu:
        "La formation continue est un pilier fondamental pour construire une carrière réussie. Cet article examine les différentes façons dont la formation continue peut contribuer à votre croissance professionnelle, des cours spécialisés aux mentorats.\n\n...",
      tags: ["carrière", "formation continue", "croissance professionnelle"]
    },
    {
      id: 3,
      titre: "L'avenir de l'apprentissage en ligne : Tendances et innovations",
      auteur: "Sophie Martin",
      date: "2023-11-18",
      contenu:
        "Explorez les tendances émergentes dans le domaine de l'apprentissage en ligne. Des technologies éducatives innovantes aux approches pédagogiques modernes, découvrez comment l'avenir de l'apprentissage en ligne façonne la formation professionnelle.\n\n...",
      tags: ["apprentissage en ligne", "innovation", "technologie éducative"]
    },
    {
      id: 4,
      titre:
        "L'équilibre entre l'éducation formelle et l'apprentissage autodidacte",
      auteur: "Alexandre Tremblay",
      date: "2023-11-20",
      contenu:
        "Comment trouver le juste équilibre entre les cursus éducatifs traditionnels et l'apprentissage autodidacte ? Cet article explore l'importance de combiner ces deux approches pour une éducation complète et adaptable.\n\n...",
      tags: [
        "éducation formelle",
        "apprentissage autodidacte",
        "équilibre éducatif"
      ]
    },
    {
      id: 5,
      titre:
        "Développer des compétences interpersonnelles : Clé du succès professionnel",
      auteur: "David Lopez",
      date: "2023-11-22",
      contenu:
        "Au-delà des compétences techniques, les compétences interpersonnelles sont cruciales dans le monde professionnel. Découvrez comment le développement de ces compétences peut influencer positivement votre carrière et vos relations professionnelles.\n\n...",
      tags: [
        "compétences interpersonnelles",
        "développement professionnel",
        "succès professionnel"
      ]
    },
    {
      id: 6,
      titre: "La transition vers une carrière technologique : Guide complet",
      auteur: "Emma Williams",
      date: "2023-11-25",
      contenu:
        "Vous envisagez une transition vers une carrière technologique ? Ce guide complet explore les étapes clés, les compétences nécessaires et les ressources disponibles pour réussir votre passage vers le secteur technologique en plein essor.\n\n...",
      tags: [
        "transition de carrière",
        "carrière technologique",
        "compétences techniques"
      ]
    },
    {
      id: 7,
      titre:
        "L'apprentissage expérientiel : Une approche pratique de la formation",
      auteur: "Antoine Lefevre",
      date: "2023-11-28",
      contenu:
        "Découvrez comment l'apprentissage expérientiel offre une approche pratique et immersive de la formation professionnelle. Des stages aux projets concrets, explorez comment cette méthode peut renforcer vos compétences de manière significative.\n\n...",
      tags: [
        "apprentissage expérientiel",
        "formation pratique",
        "immersion professionnelle"
      ]
    },
    {
      id: 8,
      titre: "L'importance du mentorat dans le développement professionnel",
      auteur: "Sophie Dubois",
      date: "2023-11-30",
      contenu:
        "Le mentorat joue un rôle crucial dans le développement professionnel. Cet article examine les avantages du mentorat, comment trouver le mentor idéal et comment cette relation peut accélérer votre croissance professionnelle.\n\n...",
      tags: [
        "mentorat",
        "développement professionnel",
        "croissance professionnelle"
      ]
    },
    {
      id: 9,
      titre: "Les soft skills : Clés du succès dans le monde du travail",
      auteur: "Pierre Martin",
      date: "2023-12-02",
      contenu:
        "Au-delà des compétences techniques, les soft skills sont devenues essentielles sur le lieu de travail. Découvrez les compétences interpersonnelles, la résolution de problèmes et la créativité, et comment les développer pour atteindre le succès professionnel.\n\n...",
      tags: [
        "soft skills",
        "compétences interpersonnelles",
        "succès professionnel"
      ]
    },
    {
      id: 10,
      titre: "Le rôle de la diversité dans la formation professionnelle",
      auteur: "Amina Bah",
      date: "2023-12-05",
      contenu:
        "Explorez l'impact positif de la diversité dans la formation professionnelle. Comment la diversité culturelle et de genre peut-elle enrichir les expériences d'apprentissage et favoriser un environnement professionnel inclusif ?\n\n...",
      tags: ["diversité", "inclusion", "formation professionnelle"]
    },
    {
      id: 11,
      titre: "Les défis de la formation à distance : Solutions pratiques",
      auteur: "Olivier Moreau",
      date: "2023-12-08",
      contenu:
        "La formation à distance présente des défis uniques. Cet article examine ces défis et propose des solutions pratiques pour garantir une expérience d'apprentissage efficace, que ce soit pour les étudiants ou les professionnels en reconversion.\n\n...",
      tags: [
        "formation à distance",
        "enseignement en ligne",
        "solutions pratiques"
      ]
    },
    {
      id: 12,
      titre:
        "Comment rester compétitif sur le marché du travail en constante évolution",
      auteur: "Caroline Dufresne",
      date: "2023-12-10",
      contenu:
        "Le marché du travail évolue rapidement, et rester compétitif nécessite une adaptabilité constante. Découvrez des stratégies pratiques pour développer vos compétences, rester à jour avec les tendances du marché et prospérer dans un environnement professionnel en constante évolution.\n\n...",
      tags: ["compétitivité", "adaptabilité", "tendances du marché du travail"]
    }
  ];

  ngOnInit(): void {
    AOS.init();
  }
}
