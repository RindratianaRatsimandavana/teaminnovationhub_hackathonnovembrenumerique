import { Injectable } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class ScrollTopService {
  constructor(private router: Router) {}
  onTop() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0); // Mettez à jour cette valeur selon votre préférence
      }
    });
  }
}
