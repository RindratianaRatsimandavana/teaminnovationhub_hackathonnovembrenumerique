import { Injectable } from "@angular/core";
import { Observable, delay, of, tap } from "rxjs";
import { UserService } from "./user.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private userData = {
    id: 1,
    nom: "Doe",
    prenom: "John",
    nomUtilisateur: "johndoe123",
    dateDeNaissance: "1990-05-15",
    numeroTelephone: "+1-555-555-5555",
    email: "john.doe@example.com",
    adresse: "123 Main Street, City, Country",
    motDePasse: "motdepasse123",
    userRole: "apprentis", //apprentis ou societe ou formateur ou visiteur
  };

  isLoggedIn: boolean = false;
  redirectUrl: string;
  USER_KEY = "auth-user";
  TYPE_USER_KEY = "type-user";

  // apprenti: apprenti;
  // societe: societe;
  // formateur: formateur;

  constructor(private userService: UserService) {}

  // login(name: string, password: string): Observable<boolean> {
  //   const isLoggedIn = name == "a" && password == "a";

  //   return of(isLoggedIn).pipe(
  //     delay(1000),
  //     tap((isLoggedIn) => {
  //       this.isLoggedIn = isLoggedIn;
  //       if (isLoggedIn) {
  //         window.sessionStorage.removeItem(this.USER_KEY);
  //         window.sessionStorage.setItem(
  //           this.USER_KEY,
  //           JSON.stringify(this.userData)
  //         );
  //       }
  //     })
  //   );
  // }

  login(name: string, password: string, type: string): any {
    this.userService.login(name, password, type).subscribe((data: any) => {
      let userConnecte = data.data as any[];
      if (userConnecte.length === 1) {
        window.sessionStorage.removeItem(this.USER_KEY);
        window.sessionStorage.setItem(
          this.USER_KEY,
          JSON.stringify(userConnecte)
        );
        this.isLoggedIn = true;
      }
    });
    return this.isLoggedIn;
    // const isLoggedIn = name == "a" && password == "a";

    // return of(isLoggedIn).pipe(
    //   delay(1000),
    //   tap((isLoggedIn) => {
    //     this.isLoggedIn = isLoggedIn;
    //     if (isLoggedIn) {
    //       window.sessionStorage.removeItem(this.USER_KEY);
    //       window.sessionStorage.setItem(
    //         this.USER_KEY,
    //         JSON.stringify(this.userData)
    //       );
    //     }
    //   })
    // );
  }

  logout() {
    this.isLoggedIn = false;
    window.sessionStorage.clear();
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }

  public getTypeUser(): any {
    return window.sessionStorage.getItem(this.TYPE_USER_KEY);
  }
}
