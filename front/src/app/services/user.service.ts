import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class UserService {
  API_URL = "https://localhost:44329/";
  constructor(private http: HttpClient) {}
  login(name: string, password: string, type: string) {
    let url: string = "";
    switch (type) {
      case "apprentis":
        url = this.API_URL + "membre/loginApprenti";
        break;
      case "societe":
        url = this.API_URL + "membre/loginSociete";
        break;
      case "formateur":
        url = this.API_URL + "membre/loginFormateur";
        break;
    }
    let user = {
      login: name,
      motDePasse: password,
    };

    return this.http.post<any>(url, user);
  }

  inscriptionApprentis(
    nom: string,
    prenom: string,
    nomUtilisateur: string,
    dateDeNaissance: string,
    numeroTelephone: string,
    email: string,
    adresse: string,
    motDePasse: string
  ) {
    let url: string = this.API_URL + "membre/inscriptionApprentis";
    let apprentis = {
      nom,
      prenom,
      nomUtilisateur,
      dateDeNaissance,
      numeroTelephone,
      email,
      adresse,
      motDePasse,
    };
    return this.http.post<any>(url, apprentis);
  }

  inscriptionSociete(
    nom: string,
    descriptionDetail: string,
    mail: string,
    motDePasse: string
  ) {
    let url: string = this.API_URL + "membre/inscriptionSociete";
    let societe = {
      nom,
      descriptionDetail,
      mail,
      motDePasse,
    };
    return this.http.post<any>(url, societe);
  }

  inscriptionFormateur(nom: string, prenom: string, idSociete: number) {
    let url: string = this.API_URL + "membre/inscriptionFormateur";
    let formateur = {
      nom,
      prenom,
      idSociete,
    };
    return this.http.post<any>(url, formateur);
  }
}
