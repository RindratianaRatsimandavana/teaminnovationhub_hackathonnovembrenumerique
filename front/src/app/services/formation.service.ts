import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, tap } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FormationService {
  constructor(private http: HttpClient) {}
  API_URL = "https://localhost:44329/";

  getAllFormation(): Observable<any> {
    return this.http.get(`${this.API_URL}formation/getAllFormation`);
  }

  getVueFormationByIdCategorie(idCategorie: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}formation/getVueFormationByIdCategorie?idCategorie=${idCategorie}`
    );
  }

  getVueFormationByIdFormationAndIdApprenti(
    idFormation: number,
    idApprentis: number
  ): Observable<any> {
    return this.http.get(
      `${this.API_URL}formation/getVueFormationById?idFormation=${idFormation}&idApprentis=${idApprentis}`
    );
  }

  getAllFormationParSociete(idSociete: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}formation/getVueFormationByIdSociete?idSociete=${idSociete}`
    );
  }

  getAllFormationParFormateur(idFormateur: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}formation/getVueFormationByIdFormateur?idFormateur=${idFormateur}`
    );
  }

  getMesFormationsParIdApprenti(idApprentis: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}formation/getMesFormationsParIdApprenti?idApprentis=${idApprentis}`
    );
  }

  getApprentisEtFormation(
    idApprentis: number,
    idFormation: number
  ): Observable<any> {
    return this.http.get(
      `${this.API_URL}formation/getApprentisEtFormation?idApprentis=${idApprentis}&idFormation=${idFormation}`
    );
  }

  insertApprentisEtFormation(idApprentis: number, idFormation: number) {
    let apprentiFormation = {
      idApprentis: idApprentis,
      idFormation: idFormation,
    };
    return this.http.post<any>(
      `${this.API_URL}formation/insertApprentisEtFormation`,
      apprentiFormation
    );
  }

  // getFormations(idUser: number): Observable<any> {
  //   return this.http.get(
  //     `${this.API_URL}accueil/getformations?iduser=${idUser}`
  //   );
  // }

  // jsonData = [
  //   {
  //     ID: 1,
  //     Titre: "Formation sur l'élevage de volailles",
  //     Description:
  //       "La formation sur l'élevage de volailles vous préparera à devenir un expert dans le domaine de l'aviculture. Vous acquerrez des compétences essentielles pour la gestion et l'élevage de volailles. Après la formation, vous pourrez rejoindre la Ferme Bemasoandro en tant qu'éleveur de volailles expérimenté.",
  //     CategorieID: 1,
  //   },
  //   {
  //     ID: 2,
  //     Titre: "Formation sur la culture de produits biologiques",
  //     Description:
  //       "La formation sur la culture de produits biologiques vous permettra de maîtriser les techniques de l'agriculture biologique. Vous contribuerez à la production de produits biologiques de haute qualité. Après la formation, vous pourrez rejoindre la Ferme Anjomakely en tant que spécialiste de la culture biologique.",
  //     CategorieID: 1,
  //   },
  //   {
  //     ID: 3,
  //     Titre: "Formation en développement web",
  //     Description:
  //       "La formation en développement web vous initiera à la création de sites web dynamiques et interactifs. Vous développerez des compétences en programmation web et en conception d'interfaces utilisateur. Après la formation, vous serez prêt à rejoindre la Société Informatix en tant que développeur web compétent.",
  //     CategorieID: 2,
  //   },
  //   {
  //     ID: 4,
  //     Titre: "Formation en sécurité informatique",
  //     Description:
  //       "La formation en sécurité informatique vous formera pour devenir un expert en protection des systèmes informatiques. Vous apprendrez à détecter et à contrer les menaces en ligne. Après la formation, vous serez prêt à rejoindre la Société Informatix en tant qu'expert en sécurité informatique.",
  //     CategorieID: 2,
  //   },
  //   {
  //     ID: 5,
  //     Titre: "Formation en réparation automobile",
  //     Description:
  //       "La formation en réparation automobile vous enseignera les compétences nécessaires pour diagnostiquer et réparer les véhicules automobiles. Vous deviendrez un mécanicien qualifié. Après la formation, vous pourrez intégrer le Garage AutoMada en tant que réparateur automobile professionnel.",
  //     CategorieID: 3,
  //   },
  //   {
  //     ID: 6,
  //     Titre: "Formation en conception mécanique",
  //     Description:
  //       "La formation en conception mécanique vous initiera à la conception de pièces mécaniques et d'assemblages. Vous développerez des compétences essentielles en conception assistée par ordinateur. Après la formation, vous serez prêt à rejoindre le Garage AutoMada en tant que concepteur mécanique.",
  //     CategorieID: 3,
  //   },
  //   {
  //     ID: 7,
  //     Titre: "Formation pour devenir enseignant en école primaire",
  //     Description:
  //       "La formation pour devenir enseignant en école primaire vous préparera à enseigner efficacement aux élèves du primaire. Vous développerez des compétences pédagogiques essentielles. Après la formation, vous pourrez intégrer l'École Excellence en tant qu'enseignant en école primaire.",
  //     CategorieID: 4,
  //   },
  //   {
  //     ID: 8,
  //     Titre: "Formation pour devenir formateur professionnel",
  //     Description:
  //       "La formation pour devenir formateur professionnel vous aidera à devenir un formateur compétent dans divers domaines. Vous développerez des compétences en formation et en communication. Après la formation, vous pourrez rejoindre l'École Excellence en tant que formateur professionnel.",
  //     CategorieID: 4,
  //   },
  // ];
}
