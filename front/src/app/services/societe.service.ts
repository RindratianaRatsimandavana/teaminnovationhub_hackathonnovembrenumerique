import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, tap } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SocieteService {
  constructor(private http: HttpClient) {}
  API_URL = "https://localhost:44329/";

  getSocieteById(idSociete: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}societe/getSocieteById?idSociete=${idSociete}`
    );
  }
}
