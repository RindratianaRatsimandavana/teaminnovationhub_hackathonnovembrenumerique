import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, tap } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FormateurService {
  constructor(private http: HttpClient) {}
  API_URL = "https://localhost:44329/";

  getFormateurByIdSociete(idSociete: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}formateur/getFormateurByIdSociete?idSociete=${idSociete}`
    );
  }

  getFormateurById(idFormateur: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}formateur/getFormateurById?idFormateur=${idFormateur}`
    );
  }

  validerFormateur(idFormateur: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}membre/validerFormateur?idAmodifier=${idFormateur}`
    );
  }
}
