import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, tap } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FAQsService {
  constructor(private http: HttpClient) {}
  API_URL = "https://localhost:44329/";

  /*  */
  getAllFAQ(): Observable<any> {
    return this.http.get(`${this.API_URL}accueil/getallfaq`);
  }
}
