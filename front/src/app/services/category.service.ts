import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, tap } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CategoryService {
  constructor(private http: HttpClient) {}
  API_URL = "https://localhost:44329/";
  jsonData = [
    {
      ID: 1,
      NomCategorie: "Agronomie",
    },
    {
      ID: 2,
      NomCategorie: "Informatique",
    },
    {
      ID: 3,
      NomCategorie: "Mécanique",
    },
    {
      ID: 4,
      NomCategorie: "Éducation",
    },
  ];
}
